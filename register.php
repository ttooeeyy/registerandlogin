<?php include('server.php'); ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>สมัครสมาชิก</title>

    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="header">
        <h2>สมัครสมาชิก</h2>
    </div>
    <form action="register_db" method="post">
       <div class="input-group">
            <label for="username">username</label> 
            <input type="text" name="username">      
       </div>
       <div class="input-group">
            <label for="email">email</label> 
            <input type="email" name="email">      
       </div>
       <div class="input-group">
            <label for="password_1">Password</label> 
            <input type="password" name="password_1">      
       </div>
       <div class="input-group">
            <label for="tel">เบอร์โทรศัพท์</label> 
            <input type="text" name="tel">      
       </div>
       <div class="input-group">
            <label for="address">ที่อยู่</label> 
            <input type="text" name="address">      
       </div>
       <div class="input-group">
            <label for="sex">เพศ</label> 
            <input type="text" name="sex">      
       </div>
       <div class="input-group">
            <label for="religion">ศาสนา</label> 
            <input type="text" name="religion">      
       </div>
       <div class="input-group">
            <label for="job">อาชีพ</label> 
            <input type="text" name="job">      
       </div>
       <div class="input-group">
             <input type="submit" name="reg_user" class="btn"></button>      
       </div>
          <p>ถ้าเป็นสมาชิกคลิกเลย  กดตรงนี้ไอดิไอเวน<a href="login.php">Sing in</a></p>
    </form>
    
    
</body>
</html>